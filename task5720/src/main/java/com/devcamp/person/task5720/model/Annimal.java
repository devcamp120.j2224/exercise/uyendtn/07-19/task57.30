package com.devcamp.person.task5720.model;

public abstract class Annimal {
    private int age;
    private String gender;

    public abstract void isMammal();

    public void mate(){
        System.out.println("annimal mating ...");
    }

    public Annimal() {
    }

    public Annimal(int age, String gender) {
        this.age = age;
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    
}
