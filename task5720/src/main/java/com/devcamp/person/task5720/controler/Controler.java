package com.devcamp.person.task5720.controler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.person.task5720.model.Address;
import com.devcamp.person.task5720.model.Annimal;
import com.devcamp.person.task5720.model.Duck;
import com.devcamp.person.task5720.model.Fish;
import com.devcamp.person.task5720.model.Person;
import com.devcamp.person.task5720.model.Profesor;
import com.devcamp.person.task5720.model.Student;
import com.devcamp.person.task5720.model.Subject;
import com.devcamp.person.task5720.model.Worker;
import com.devcamp.person.task5720.model.Zebra;

@RestController
public class Controler {
    @CrossOrigin
    @GetMapping("listPerson")
    public List<Person> listPerson(@RequestParam(name = "type", required = false) String type) {
        ArrayList<Person> lstPerson = new ArrayList<Person>();

        Address address1 = new Address("Mandison", "Miami", "US", 12314);
        Address address2 = new Address("Mandison", "Miami", "US", 12314);

        ArrayList<Annimal> lstAnimal = new ArrayList<Annimal>();

        Annimal duck = new Duck();
        duck.setAge(2);
        duck.setGender("male");
        ((Duck)duck).setBeakColor("yellow");
        duck.mate();
        duck.isMammal();

        Annimal fish = new Fish(3, "female", 3, true);
        fish.mate();
        fish.isMammal();

        Annimal zebra  = new Zebra(true);
        zebra.setAge(4);
        zebra.setGender("male");
        ((Zebra)zebra).run();

        lstAnimal.add(duck);
        lstAnimal.add(fish);
        lstAnimal.add(zebra);

        Person profesorMath = new Profesor();
         profesorMath.setAge(50);
         profesorMath.setGender("male");
         profesorMath.setName("John");
        // profesorMath.setAddress(address1);
         ((Profesor) profesorMath).teaching();
         profesorMath.setListPet(lstAnimal);

        Person worker1 = new Worker();
         worker1.setAge(50);
         worker1.setName("Dave");
         worker1.setGender("male");
         worker1.setListPet(lstAnimal);

        ArrayList<Subject> listSubject = new ArrayList<Subject>();
        Subject subjectMath = new Subject("Math", 1, (Profesor) profesorMath);
        listSubject.add(subjectMath);

        
        Person student1 = new Student();
         ((Student)student1).setStudentId(1);
         ((Student)student1).setSubject(listSubject);
         student1.setAge(20);
         student1.setGender("male");
         student1.setName("John");
         student1.setListPet(lstAnimal);
       // lstPerson.add(student1);
        // lstPerson.add(profesorMath);
        // lstPerson.add(worker1);
        List<Person> list = new ArrayList<>();
         
        if(type.equals("student")){
            list.add(student1);
        } else if(type.equals("profesor")){
            list.add(profesorMath);
        } else if(type.equals("worker")){
            list.add(worker1);
        } else {
            list.add(student1);
            list.add(profesorMath);
            list.add(worker1);
        }
        return list;
    }

}
